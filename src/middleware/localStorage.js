import { setLocalData } from '../utils/localStorage'
const localStorageMiddleware = store => next => action => {
    const state = store.getState();
    next(action)
    setLocalData(state)
}

export default localStorageMiddleware