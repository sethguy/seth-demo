export const localKey = 'seth.demo.time';
export const getLocalData = () => {

    const storageString = localStorage.getItem(localKey);

    let localData = {

    }

    if (storageString) {
        try {
            const data = JSON.parse(storageString);
            localData = {
                ...data
            }
        } catch (error) {

        }
    }
    return localData;
}

export const setLocalData =(data) =>{
    const dataString = JSON.stringify(data);
    localStorage.setItem(localKey,dataString)
}