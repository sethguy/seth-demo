import React, { Component } from 'react';
import { loadEvents } from '../../actions/events'
import { connect } from 'react-redux';
import * as fire from '../../actions/firebaseObjects'
import { DebounceInput } from 'react-debounce-input';
import CakeLine from '../cakeLine'
const collectionName = 'cakes';
class Home extends Component {
    componentWillMount() {
        this
            .props
            .loadEvents({})
        this.props.getFirebaseObjects(collectionName, {})
    }
    render() {
        return (
            <div className="h-100 d-flex flex-column" >
                <div className="d-flex justify-content-center"  >
                    <h1>
                        Home
                    </h1>
                </div>
                <div className="d-flex justify-content-center"  >
                    <p>
                        {this.props.eventData.name}
                    </p>
                </div>
                <div className="d-flex"  >
                    <button
                        onClick={() => this.props.addFirebaseObject(collectionName, { name:'' })}
                        className='btn btn-info m-2'>
                        Add Cake
                    </button>
                </div>
                <div className="flex-1"  >
                    {
                        this.props.cakes.map(cake => {
                            return (<CakeLine key ={cake.id} {...{collectionName,...cake}} />)
                        })
                    }
                </div>
            </div>
        );
    }
}

const defaultEvent = {};

const mapStateToProps = ({ events, firebaseObjects }) => ({
    eventData: events.eventData || defaultEvent,
    cakes: firebaseObjects.cakes || []
})

export default connect(mapStateToProps, {
    loadEvents,
    ...fire
})(Home)
