import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as fire from '../actions/firebaseObjects'
import { DebounceInput } from 'react-debounce-input';

const CakeLine = (props) => {

    return (
        <div className="d-flex align-items-center"  >
            <button
                onClick={() => props.removeFirebaseObject(props.collectionName, props.id)}
                className='btn btn-danger m-2'>
                delete
            </button>
            <DebounceInput
                debounceTimeout={500}
                value={props.name}
                onChange={(event) => props.editFirebaseObject(props.collectionName, { id: props.id, name: event.target.value })}
                className='form-control input-lg flex-1 m-1'
            />
        </div>
    );

}

const mapStateToProps = ({ }) => ({
})

export default connect(mapStateToProps, {
    ...fire
})(CakeLine)
