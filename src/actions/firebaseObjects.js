import firebase from '../firebase';
import { v4 } from 'uuid';

const db = firebase.firestore();

export const setFirebaseObjects = (collectionName, firebaseObjects) => ({ type: 'setFirebaseObjects', collectionName, firebaseObjects })

export const addFirebaseObject = (collectionName, data) => async (dispatch, getState) => {

    const firebaseObjectsCollectionRef = db.collection(collectionName);

    const firebaseObject = {
        id: v4(),
        ...data
    }

    const docRef = firebaseObjectsCollectionRef.doc(firebaseObject.id)
    await docRef.set({ ...data })

    return dispatch({ type: "addFirebaseObject", collectionName, firebaseObject });
}

export const removeFirebaseObject = (collectionName, id) => async (dispatch, getState) => {
    const firebaseObjectsCollectionRef = db.collection(collectionName);

    const docRef = firebaseObjectsCollectionRef.doc(id)
    await docRef.delete()
    return dispatch({ type: "removeFirebaseObject", collectionName, id });
}

export const editFirebaseObject = (collectionName, update) => async (dispatch, getState) => {
    const firebaseObjectsCollectionRef = db.collection(collectionName);

    const { id, ...data } = update;
    const docRef = firebaseObjectsCollectionRef.doc(update.id)
    await docRef.update({
        ...data
    })
    return dispatch({ type: "editFirebaseObject", collectionName, update });
}

export const getFirebaseObjects = (collectionName, params) => async (dispatch, getState) => {
    const firebaseObjectsCollectionRef = db.collection(collectionName);

    dispatch({ type: "getFirebaseObjects", collectionName, params });
    const result = await firebaseObjectsCollectionRef.get();

    const { docs } = result;

    const firebaseObjects = docs.map(doc => ({
        ...doc.data(),
        id: doc.id,
    }))

    return dispatch(setFirebaseObjects(collectionName, firebaseObjects));
}

