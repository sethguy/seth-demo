import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../../actions/firebaseObjects';
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares)

const collectionName = 'documents';

describe('setFirebaseObjects', () => {
  it('should setFirebaseObjects', () => {
    const firebaseObjects = [{ id: '1' }]
    const expectedAction = {
      type: 'setFirebaseObjects',
      collectionName,
      firebaseObjects,
    }
    expect(actions.setFirebaseObjects(collectionName, firebaseObjects)).toEqual(expectedAction)
  })
})

describe('addFirebaseObject', () => {

  it('should addFirebaseObject', () => {

    const store = mockStore({})

    const data = { name: 'doc1' }

    return store.dispatch(actions.addFirebaseObject(collectionName, data)).then(() => {
      expect(store.getActions()).toEqual([{ type: 'addFirebaseObject', collectionName, firebaseObject: { "id": '1', "name": "doc1" } }])
    })
  })
})


describe('removeFirebaseObject', () => {

  it('should removeFirebaseObject', () => {

    const store = mockStore({})
    const id = '1'
    return store.dispatch(actions.removeFirebaseObject(collectionName, id)).then(() => {
      expect(store.getActions()).toEqual([{ type: 'removeFirebaseObject', collectionName, id: '1' }])
    })
  })
})


describe('editFirebaseObject', () => {

  it('should editFirebaseObject', () => {

    const store = mockStore({})
    const id = '1'

    const update = {
      id,
      name: 'doc3',
    }
    return store.dispatch(actions.editFirebaseObject(collectionName, update)).then(() => {
      expect(store.getActions()).toEqual([{ type: 'editFirebaseObject', collectionName, update }])
    })
  })
})

describe('getFirebaseObjects', () => {

  it('should getFirebaseObjects', () => {

    const store = mockStore({})

    const params = {

    }
    return store.dispatch(actions.getFirebaseObjects(collectionName, params)).then(() => {
      expect(store.getActions()).toEqual([
        {
          collectionName,
          params,
          "type": "getFirebaseObjects"
        },
        {
          collectionName,
          "firebaseObjects": [{ "id": "1" }],
          "type": "setFirebaseObjects"
        }
      ])
    })
  })
})
