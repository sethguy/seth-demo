import { getQueryParams, getPathVariables } from '../../utils/location'

delete window.location;
window.location = {
    href: "https://seth-demo-time.firebaseapp.com/#/test?key=value"
};
class MockDate {
    static watch = jest.fn();
    constructor() {
        MockDate.watch()
    }
}
global.Date = MockDate

describe('getQueryParams', () => {
    it('should getQueryParams', () => {
        const params = getQueryParams();
        expect(params).toEqual({"key": "value"})
    })
})

describe('getPathVariables', () => {
    it('should getPathVariables', () => {
        const params = getPathVariables();
        expect(params).toEqual(["", "test"])
    })
})
