import { getLocalData, setLocalData,localKey } from '../../utils/localStorage'

delete global.localStorage


const MockLocalStorage = {
    getItem: jest.fn(() => "{\"key\":\"value\"}"),
    setItem: jest.fn()
}
global.localStorage = MockLocalStorage

describe('getQueryParams', () => {
    it('should getQueryParams', () => {
        const data = getLocalData();
        expect(localStorage.getItem).toBeCalledWith(localKey)

        expect(data).toMatchObject({"key": "value"})

    })
})

describe('getPathVariables', () => {
    it('should getPathVariables', () => {
        const data = { key: 'value' };
        setLocalData(data);
        expect(localStorage.setItem).toBeCalledWith(localKey,"{\"key\":\"value\"}")
    })
})
