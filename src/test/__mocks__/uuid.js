const v4 = jest.fn(() => '1')
const uuid = {
    v4,
}
module.exports = uuid;