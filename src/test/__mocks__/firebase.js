const { v4 } = require('uuid');

const initializeApp = jest.fn()

const mockFirestoreDoc = {
    set: jest.fn(),
    delete: jest.fn(),
    update: jest.fn(),
    data: jest.fn(() => ({})),
    id: v4()
}

const mockFirestoreCollection = {
    doc: jest.fn(() => mockFirestoreDoc),
    get: jest.fn(() => Promise.resolve({ docs: [mockFirestoreDoc] }))
}

const mockFirestore = {
    collection: jest.fn(() => mockFirestoreCollection)
}

const firestore = jest.fn(() => mockFirestore);

const firebase = {
    initializeApp,
    firestore,
}


module.exports = firebase;