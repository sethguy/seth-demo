import localStorageMiddleware from '../../middleware/localStorage'
import { setLocalData } from '../../utils/localStorage'

jest.mock('../../utils/localStorage',()=>{
    return {
        setLocalData:jest.fn()
    }
})

it('should set local data on any action', () => {
    const nextArgs = [];
    const fakeNext = (...args) => { nextArgs.push(args); };
    const getState = jest.fn(()=>'state')
    const fakeStore = {getState};
    const action = { type: 'any' };
    localStorageMiddleware(fakeStore)(fakeNext)(action);
    expect(setLocalData).toBeCalledWith('state')
    expect(getState).toBeCalled()
});