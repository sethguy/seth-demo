import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import reducer from '../../reducers/firebaseObjects'
import { addFirebaseObject, setFirebaseObjects, removeFirebaseObject,editFirebaseObject } from '../../actions/firebaseObjects'
const collectionName = 'documents';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares)

const store = mockStore({})

describe('firebaseObjects reducer', () => {
  it('should return the initial state', () => {
    expect(reducer({}, {})).toEqual({})
  })
})

describe('addFirebaseObject case', () => {
  it('should should handle addFirebaseObject', async (done) => {
    const data = { name: 'doc1' }
    const addFirebaseObjectAction = await store.dispatch(addFirebaseObject(collectionName, data))
    const reducerResult = reducer({}, addFirebaseObjectAction)
    expect(reducerResult).toEqual({ documents: [{ id: '1', name: 'doc1' }] })
    done()
  })
})


describe('setFirebaseObjects case', () => {
  it('should should handle setFirebaseObjects', () => {
    const firebaseObjects = [{ id: '1' }]
    const setFirebaseObjectsAction = store.dispatch(setFirebaseObjects(collectionName, firebaseObjects))
    const reducerResult = reducer({}, setFirebaseObjectsAction)
    expect(reducerResult).toEqual({ documents: [...firebaseObjects] })
  })
})


describe('removeFirebaseObject case', () => {
  it('should should handle removeFirebaseObject', async (done) => {
    const firebaseObjects = [{ id: '1' }, { id: '2' }]
    const id = '1'
    const removeFirebaseObjectAction = await store.dispatch(removeFirebaseObject(collectionName, id))
    const reducerResult = reducer({ documents: [...firebaseObjects] }, removeFirebaseObjectAction)
    expect(reducerResult).toEqual({ documents: [{ id: '2' }] })
    done()
  })
})

describe('editFirebaseObject case', () => {
  it('should should handle editFirebaseObject', async (done) => {
    const firebaseObjects = [{ id: '1',name:'doc1' }, { id: '2' }]
    const id = '1';
    const update = {
      id,
      name: 'doc3',
    }
    const editFirebaseObjectAction = await store.dispatch(editFirebaseObject(collectionName, update))
    const reducerResult = reducer({ documents: [...firebaseObjects] }, editFirebaseObjectAction)
    expect(reducerResult).toEqual({ documents: [{ id: '1',name:'doc3' }, { id: '2' }] })
    done()
  })
})