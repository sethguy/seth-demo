import reducer from '../../reducers/location'

delete window.location;
window.location = { 
  href:"https://seth-demo-time.firebaseapp.com/#/test?key=value"
};
class MockDate {
 static watch = jest.fn();
    constructor(){
      MockDate.watch()
    }
}
global.Date = MockDate

jest.mock('../../utils/location',()=>{
  return {
    getQueryParams:jest.fn(()=>'getQueryParams'),
    getPathVariables:jest.fn(()=>'getPathVariables'),

  }
})

describe('todos reducer', () => {
  it('should return location data', () => {
    expect(reducer({}, {})).toEqual({
    "href": "https://seth-demo-time.firebaseapp.com/#/test?key=value",
    "params": "getQueryParams",
    "paths": "getPathVariables",
    "timeStamp": new Date(),
    })
  })
})
