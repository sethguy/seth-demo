import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow'
import { Provider } from 'react-redux'

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import Cake from '../../../components/cakeLine'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares)
const collectionName = 'cakes';
const cake = { id: '1', name: 'cheese' }

const ProviderWrapper = ({ store }) => {

  return (
    <Provider store={store}>
      <Cake {...{ collectionName, ...cake }} />
    </Provider>
  )
}

describe("Cake", () => {
  test("it matches the snapshot", () => {
    const props = {}
    const renderer = new ShallowRenderer()
    const result = renderer.render(<ProviderWrapper store={mockStore({})} />)
    expect(result).toMatchSnapshot()
  });
});