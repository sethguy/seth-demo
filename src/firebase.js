import firebase from "firebase";

// See firebase setup in above google firebase documentation url
export const config = {
    apiKey: "AIzaSyA0nre4GZ7YChCXbsRKfaRyRRgwn6Ut_xs",
    authDomain: "seth-demo-time.firebaseapp.com",
    databaseURL: "https://seth-demo-time.firebaseio.com",
    projectId: "seth-demo-time",
    storageBucket: "",
    messagingSenderId: "495527428305",
    appId: "1:495527428305:web:df107f5f585dfe0c"
};

firebase.initializeApp(config);

export default firebase;