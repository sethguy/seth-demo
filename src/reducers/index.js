import { combineReducers } from 'redux'

import locationUtil from './location'
import events from './events'
import firebaseObjects from './firebaseObjects'

export const rootReducer = combineReducers({
  locationUtil,
  events,
  firebaseObjects,
})