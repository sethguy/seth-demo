
const firebaseObjects = (state = {}, action) => {
    const { collectionName } = action;
    const collectionSlice = state[collectionName] || [];

    switch (action.type) {
        case 'addFirebaseObject': {
            return {
                ...state,
                [collectionName]: [
                    ...collectionSlice,
                    { ...action.firebaseObject }
                ]
            }
        }
        case 'setFirebaseObjects': {
            return {
                ...state,
                [collectionName]: [...action.firebaseObjects],
            }
        }
        case 'removeFirebaseObject': {
            return {
                ...state,
                [collectionName]: collectionSlice.filter(firebaseObject => firebaseObject.id !== action.id)
            }
        }
        case 'editFirebaseObject': {
            return {
                ...state,
                [collectionName]: collectionSlice.map(firebaseObject => {
                    if (firebaseObject.id === action.update.id) {
                        return {
                            ...firebaseObject,
                            ...action.update,
                        }
                    }
                    return firebaseObject;
                })
            }
        }
        default:
            return state;
    }
}


export default firebaseObjects