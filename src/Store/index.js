import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'

import { rootReducer } from '../reducers/'
import { getLocalData } from '../utils/localStorage';
import localStorageMiddleWare from '../middleware/localStorage'

const configureStore = preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(localStorageMiddleWare,thunk, ),
    )
  )
  return store
}

const preloadedState ={
  ...getLocalData()
}

export const store = configureStore(preloadedState);
